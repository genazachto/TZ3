def read(n):
    with open(n) as f:
        for line in f:
            x = line
    return x


def max_of_n(x):
    x = x.split(' ')
    for i in range(0, len(x)):
        if x[i].find('.') == -1:
            x[i] = int(x[i])
        else:
            x[i] = float(x[i])
    x.sort()
    return x[-1]


def min_of_n(x):
    x = x.split(' ')
    for i in range(0, len(x)):
        if x[i].find('.') == -1:
            x[i] = int(x[i])
        else:
            x[i] = float(x[i])
    x.sort()
    return x[0]


def sum_of_n(x):
    x = x.split(' ')
    s = 0
    for i in range(0, len(x)):
        if x[i].find('.') == -1:
            x[i] = int(x[i])
        else:
            x[i] = float(x[i])
        s = s + x[i]
    return s


def multiply_of_n(x):
    x = x.split(' ')
    p = 1
    for i in range(0, len(x)):
        if x[i].find('.') == -1:
            x[i] = int(x[i])
        else:
            x[i] = float(x[i])
        p = p * x[i]
    return p
