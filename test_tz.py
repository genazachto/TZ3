import os
import sys
import random
import time
from math import prod

import memory_profiler
from memory_profiler import profile

from tz import max_of_n, min_of_n, sum_of_n, multiply_of_n, read


def test_max_of_n():
    for t in range(0, 300):
        a_of_n = []
        for i in range(0, 100):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        assert max(a_of_n) == max_of_n(read('test.txt'))


def test_min_of_n():
    for t in range(0, 300):
        a_of_n = []
        for i in range(0, 100):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        assert min(a_of_n) == min_of_n(read('test.txt'))


def test_sum_of_n():
    for t in range(0, 300):
        a_of_n = []
        for i in range(0, 100):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        assert sum(a_of_n) == sum_of_n(read('test.txt'))


def test_multiply_of_n():
    for t in range(0, 300):
        a_of_n = []
        for i in range(0, 100):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        assert prod(a_of_n) == multiply_of_n(read('test.txt'))


def test_speed():
    a_of_n = []
    print('\n')
    for t in range(0, 100):
        for i in range(0, 700):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        with open('test.txt') as f:
            f.seek(0, os.SEEK_END)
            t0 = time.time()
            k = read('test.txt')
            max_of_n(k)
            min_of_n(k)
            sum_of_n(k)
            multiply_of_n(k)
            print('Время работы программы: ', time.time() - t0, 'Размер файла: ', f.tell())


def test_memory():
    a_of_n = []
    print('\n')
    for t in range(0, 100):
        for i in range(0, 700):
            if random.randint(0, 1) == 0:
                a_of_n.append(random.randint(-10000, 10000))
            else:
                a_of_n.append(random.uniform(-10000.0, 10000.0))
        with open('test.txt', 'w') as f:
            s = ''
            for i in range(0, len(a_of_n)):
                if i != len(a_of_n) - 1:
                    s = s + str(a_of_n[i]) + ' '
                else:
                    s = s + str(a_of_n[i])
            f.write(s)
        with open('test.txt') as f:
            m0 = memory_profiler.memory_usage()
            k = read('test.txt')
            max_of_n(k)
            min_of_n(k)
            sum_of_n(k)
            multiply_of_n(k)
            print(memory_profiler.memory_usage()[0] - m0[0])
